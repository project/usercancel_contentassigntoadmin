This module gives a feature to assign the content to administrator user
If you are going to cancel the account of user.

Steps:
1. Enable the module.
2. You will have one more option(5th) with the list of cancel options.
3. Option text will be "Delete the account and make its content belong 
to Administrator.".
4. If you want to assign the content to specific admin user, choose the option,
 select a admin user and cancel the account of selected user.
